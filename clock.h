#ifndef CLOCK_H
#define CLOCK_H

//*********************************************************************************
// Headers
//*********************************************************************************
#include <avr/io.h>

//*********************************************************************************
// Macros and Globals
//*********************************************************************************
typedef enum { AMPM = 12, MIL = 23 } Format;
struct CLOCK_DATA
{
    int8_t seconds;
    volatile int8_t minutes;
    volatile int8_t hours;
    uint8_t ampm;
    Format format;
};

//*********************************************************************************
// Prototypes
//*********************************************************************************
void increase_second(void);
void fix_time(void);
int8_t get_hours(void);
int8_t get_minutes(void);

void increase_d1(void);
void increase_d2(void);
void increase_d3(void);
void increase_d4(void);

#endif 
 
