#include "clock.h"

struct CLOCK_DATA clock = {0,0,7,0,AMPM};

void increase_second(void) {
    clock.seconds++;
    if(clock.seconds >= 60) {
        clock.seconds = 0;
        clock.minutes++;
    }
    fix_time();
}

void fix_time(void) {
    if(clock.minutes >= 60){
        clock.minutes -= 60;
        clock.hours++;
    }
    if(clock.minutes < 0){
        clock.minutes += 60;
        clock.hours--;
    }
    if(clock.hours > clock.format){
        if(clock.hours < MIL) {
            clock.ampm++;
            clock.hours = 1;
        } else {
            clock.hours = 0;
        }
    }
    if(clock.hours < 0){
        if(clock.hours == 12) clock.ampm--;
        clock.hours = clock.format;
    }
}

int8_t get_hours(void) {
    return clock.hours;
}

int8_t get_minutes(void) {
    return clock.minutes;
}

void increase_d1(void) {
    clock.minutes++;
}

void increase_d2(void) {
    clock.minutes += 10;
}

void increase_d3(void) {
    clock.hours++;
}

void increase_d4(void) {
    clock.hours += 10;
}
