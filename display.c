#include "display.h"

struct DISPLAY_DATA Display = {{0x30,0x6D,0x33,0x70}};
uint8_t Numbers[] = {0x7E,0x30,0x6D,0x79,0x33,0x5B,0x5F,0x70,0x7F,0x7B,0x0D,0x19,0x23,0x01,0x0F,0x00};


void SPI0_init(void) {
    PORTA.DIR |= PIN1_bm | PIN3_bm;
    PORTC.DIR = PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm;
    SPI0.CTRLB = SPI_SSD_bm;
    SPI0.CTRLA = SPI_DORD_bm | SPI_MASTER_bm | SPI_ENABLE_bm;
}

void update_data(void) {
    // SPI0.DATA = ~Display.d[0];
    // PORTC.OUT = ~1;
    // _delay_ms(1);
    // SPI0.DATA = ~Display.d[1];
    // PORTC.OUT = ~2;
    // _delay_ms(1);
    // SPI0.DATA = ~Display.d[2];
    // PORTC.OUT = ~4;
    // _delay_ms(1);
    // SPI0.DATA = ~Display.d[3];
    // PORTC.OUT = ~8;
    // _delay_ms(1);

    uint8_t show = 0xFE;
    for(int i = 0; i < 4; i++){
        PORTC.OUT = 0xFF;
        SPI0.DATA = ~Display.d[i];
        while (!(SPI0.INTFLAGS & SPI_IF_bm)){;}
        PORTC.OUT = show;
        _delay_us(250);
        show = (show<<1) + 1;
    }
}

void set_display(uint8_t display, uint8_t value) {
    Display.d[display] = value;
}

void set_display_data(uint8_t data, uint8_t pos) {
    uint8_t val1 = data/10;
    uint8_t val0 = data%10;
    Display.d[pos] = Numbers[val0];
    if(val1 == 0 && pos == 2) Display.d[pos+1] = Numbers[15];
    else Display.d[pos+1] = Numbers[val1];
}
