/*
 * Reloj using rtc
 * V0.1
*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "display.h"
#include "clock.h"

void update() {
    uint8_t hours = get_hours();
    uint8_t minutes = get_minutes();
    set_display_data(hours,2);
    set_display_data(minutes,0);
}

int main(void){
    _PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_CLKSEL_OSC20M_gc);
    SPI0_init();
    // pins
    PORTB.DIRCLR = PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm;
    PORTB.PIN0CTRL = PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
    PORTB.PIN1CTRL = PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
    PORTB.PIN2CTRL = PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
    PORTB.PIN3CTRL = PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
    sei();
    while (1) {
        update();
        update_data();
    }
    return 1;
}

ISR(PORTB_PORT_vect) {
    if( PORTB.INTFLAGS & PIN0_bm) {
        increase_d1();
        PORTB.INTFLAGS &= PIN0_bm;
    }
    if( PORTB.INTFLAGS & PIN1_bm) {
        increase_d2();
        PORTB.INTFLAGS &= PIN1_bm;
    }
    if( PORTB.INTFLAGS & PIN2_bm) {
        increase_d3();
        PORTB.INTFLAGS &= PIN2_bm;
    }
    if( PORTB.INTFLAGS & PIN3_bm) {
        increase_d4();
        PORTB.INTFLAGS &= PIN3_bm;
    }
    fix_time();
}
